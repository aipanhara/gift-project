## About The Project
![alt text](https://scontent.fpnh9-1.fna.fbcdn.net/v/t1.0-9/65630181_2157810634512189_1858046721045561344_n.jpg?_nc_cat=109&_nc_oc=AQms-TxlNEqDiXUKKA7OgKEyvhdep-IaiLPhk-rGbF3hctrDAbZY2c9u2Gcti39gQUc&_nc_ht=scontent.fpnh9-1.fna&oh=5a43f0313163c0dbe9b81d391ed8d9d9&oe=5D7C23F3 "Logo Title Text 1")

An online gift shop for awesome people. At Loyosja, we offer you only the awesome and unique gifts for your special occassions. 
Explore the collection of amazing products to surprise your loved one and make your special day even more special.

### Built With
The loyosja web application are built from these framework below

* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Ruby On Rails](https://rubyonrails.org/)

### Installation

#### Clone the repo
```sh
git clone https://aipanhara@bitbucket.org/aipanhara/gift-project.git
```
#### Install Dependencies
```sh
bundle install
```

#### copy the text from .env.example and create new file called .env paste the text and change your priority.

#### Run rake command
```sh
rake db:migrate:reset && rake db:reset
```

#### Run rails server
```sh
rails server or rails s
```

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

Your Name - [@your_twitter](https://twitter.com/your_username) - email@example.com

Project Link: [https://github.com/your_username/repo_name](https://github.com/your_username/repo_name)

