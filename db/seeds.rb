admin = CreateAdminService.new.call
user = CreateUserService.new.call

puts 'Created Admin Username' << admin.username
puts 'CREATED  Role: ' << admin.role

puts 'Created  Username' << user.phone_number
puts 'Create Role: ' << user.role

category = Category.create!([{
  category_name: "Gift",
  admin_id: 1
}])

subcategory = SubCategory.create!([{
  sub_category_name: "Gift Sub",
  admin_id: 1
}])

20.times do |p|
  p = Product.create!([{
    product_name: Faker::Lorem.sentence(2),
    product_price: 100,
    stock_quantity: 20,
    remaining_stock_quantity: 20,
    category_id: 1,
    sub_category_id: 1,
    admin_id: 1,
  }])
end
