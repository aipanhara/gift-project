class AddUserIdToBackups < ActiveRecord::Migration[5.1]
  def change
    add_column :backups, :user_id, :integer
  end
end
