class CreateMaintains < ActiveRecord::Migration[5.1]
  def change
    create_table :maintains do |t|
      t.boolean :under_maintain
      t.integer :setting_id
      t.timestamps
    end
  end
end
