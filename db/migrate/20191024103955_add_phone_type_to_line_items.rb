class AddPhoneTypeToLineItems < ActiveRecord::Migration[5.1]
  def change
    add_column :line_items, :phone_type, :string
  end
end
