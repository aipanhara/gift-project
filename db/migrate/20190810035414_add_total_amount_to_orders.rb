class AddTotalAmountToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :total_amount, :decimal, precision: 15, scale: 2, default: 0
  end
end
