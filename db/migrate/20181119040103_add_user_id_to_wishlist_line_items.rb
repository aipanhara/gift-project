class AddUserIdToWishlistLineItems < ActiveRecord::Migration[5.1]
  def change
    add_column :wishlist_line_items, :user_id, :integer
  end
end
