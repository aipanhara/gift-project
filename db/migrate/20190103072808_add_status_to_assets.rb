class AddStatusToAssets < ActiveRecord::Migration[5.1]
  def change
    add_column :assets, :status, :boolean
  end
end
