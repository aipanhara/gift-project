class AddEditQuantityToProducts < ActiveRecord::Migration[5.1]
  def change
    add_column :products, :edit_quantity, :integer, default: 0
  end
end
