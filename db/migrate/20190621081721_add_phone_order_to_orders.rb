class AddPhoneOrderToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :phone_order, :string
  end
end
