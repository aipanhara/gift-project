class AddAdminIdToSubCategories < ActiveRecord::Migration[5.1]
  def change
    add_column :sub_categories, :admin_id, :integer
  end
end
