class AddSoldDateToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :sold_date, :date
  end
end
