class AddProductIdToBackups < ActiveRecord::Migration[5.1]
  def change
    add_column :backups, :product_id, :integer
  end
end
