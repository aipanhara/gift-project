class CreateProductTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :product_types do |t|
      t.string :type_name
      t.string :slug
      t.integer :admin_id

      t.timestamps
    end
  end
end
