class AddPhoneNumToOrder < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :phone_num, :string
  end
end
