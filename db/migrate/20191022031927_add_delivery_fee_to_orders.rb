class AddDeliveryFeeToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :delivery_fee, :decimal, precision: 15, scale: 2, default: 0
  end
end
