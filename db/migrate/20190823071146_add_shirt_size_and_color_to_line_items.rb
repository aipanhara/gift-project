class AddShirtSizeAndColorToLineItems < ActiveRecord::Migration[5.1]
  def change
    add_column :line_items, :shirt_size, :string
    add_column :line_items, :color, :string
  end
end
