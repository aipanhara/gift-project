class CreateBackups < ActiveRecord::Migration[5.1]
  def change
    create_table :backups do |t|
      t.integer :order_id
      t.integer :cart_id
      t.integer :quantity

      t.timestamps
    end
  end
end
