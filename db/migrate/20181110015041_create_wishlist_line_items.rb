class CreateWishlistLineItems < ActiveRecord::Migration[5.1]
  def change
    create_table :wishlist_line_items do |t|
      t.integer :quantity
      t.integer :product_id
      t.integer :wishlist_id
      t.integer :order_id

      t.timestamps
    end
  end
end
