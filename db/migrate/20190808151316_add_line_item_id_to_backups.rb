class AddLineItemIdToBackups < ActiveRecord::Migration[5.1]
  def change
    add_column :backups, :line_item_id, :integer
  end
end
