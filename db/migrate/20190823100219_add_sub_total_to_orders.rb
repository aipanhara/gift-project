class AddSubTotalToOrders < ActiveRecord::Migration[5.1]
  def change
    add_column :orders, :sub_total, :decimal, precision: 15, scale: 2, default: 0
  end
end
