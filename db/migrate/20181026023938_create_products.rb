class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.string :product_name
      t.decimal :product_price, precision: 15, scale: 2, default: 0
      t.integer :admin_id
      t.string :slug

      t.timestamps
    end
  end
end
