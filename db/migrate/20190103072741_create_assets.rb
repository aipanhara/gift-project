class CreateAssets < ActiveRecord::Migration[5.1]
  def change
    create_table :assets do |t|
      t.string :image
      t.integer :product_id

      t.timestamps
    end
  end
end
