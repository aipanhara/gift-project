class CreateAdminService
  def call
    admin = Admin.find_or_create_by!(username: Rails.application.secrets.username) do |admin|
        admin.password = Rails.application.secrets.admin_password
        admin.password_confirmation = Rails.application.secrets.admin_password
        admin.role = Rails.application.secrets.admin_role
      end
  end
end
