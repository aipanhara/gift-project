module ApplicationHelper
  def time_format(datetime)
    datetime.strftime('%H:%M') unless datetime.blank?
  end

  def logged_in_using_omniauth?
    session[:logged_in_using_omniauth].present?
  end

  def first_cap_letter(string)
    string.split(' ').map{ |w| w.capitalize }.join(' ')
  end

end
