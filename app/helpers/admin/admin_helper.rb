module Admin::AdminHelper

  def total_order(order)
    order = Order.where(status: true)
    number_to_currency(order.sum(:total_amount))
  end

  def report_check_the_quantity(item, product)
    if item.order.status == true
      product.stock_quantity - item.quantity
    else
      product.stock_quantity
    end
  end

  def custom_flash_message
    flash_messages = []
    flash.each do |type, message|
      type = 'success' if type == 'notice'
      type = 'error' if type == 'alert'
      text = "<script>toastr.#{type}('#{message}');</script>"
      flash_messages << text.html_safe if message
    end
    flash_messages.join("\n").html_safe
  end

  def status_converter(status, correct: 'active', incorrect: 'inactive')
    if status
     correct
    else
      incorrect
    end
  end

  def order_status_converter(status, correct: 'Delivered', incorrect: 'In Progress')
    if status
     correct
    else
      incorrect
    end
  end

  def time_ago(time)
    "#{time_ago_in_words(time)} ago"
  end

  def title(blog_title)
    content_for(:title) {blog_title}
  end

  def meta_description(blog_text)
    content_for(:meta_description) {blog_text}
  end

  def image_og(image_og)
    content_for(:image_og) { image_og }
  end

  def url_og(url_og)
    content_for(:url_og){ url_og }
  end

  def markdown(text)
    options = {
      filter_html:     false,
      hard_wrap:       true,
      link_attributes: { rel: 'nofollow', target: "_blank" },
      space_after_headers: true,
      fenced_code_blocks: true
    }

    extensions = {
      autolink:           true,
      superscript:        true,

    }

    renderer = Redcarpet::Render::HTML.new(options)
    markdown = Redcarpet::Markdown.new(renderer, extensions)
    if text.present?
      markdown.render(text).html_safe
    end
  end
end
