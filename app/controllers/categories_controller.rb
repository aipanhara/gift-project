class CategoriesController < FrontEndApplication
  def show
    @category = Category.find_by_slug(params[:id])
  end
end
