class CartsController < FrontEndApplication
  before_action :authenticate_user!, only: [:show, :destroy]

  def show
    @search = Product.ransack(params[:search])
    @products = @search.result
    @cart = @current_cart
    @categories = Category.all
  end

  def destroy
    @cart = @current_cart
    @cart.destroy
    session[:cart_id] = nil
    redirect_to root_path
  end

  private
    def cart_params
      params.require(:cart).permit(:user_id)
    end
end
