class WishlistsController < FrontEndApplication
  before_action :authenticate_user!, only: [:show, :destroy]

  def show
    @wishlist = @current_wishlist
    @categories = Category.all
  end

  def destroy
    @wishlist = @current_wishlist
    @wishlist.destroy
    session[:wishlist_id] = nil
    redirect_to root_path
  end

  private
    def wishlist_params
      params.require(:wishlist).permit(:user_id)
    end
end
