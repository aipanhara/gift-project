class UsersController < Devise::RegistrationsController
  layout "application"
  before_action :set_cart
  def verify
    @search = Product.ransack(params[:search])
    @products = @search.result.includes(:category, :sub_category, :product_type)
    @categories = Category.all
    @user = User.last
    # Use Authy to send the verification token
    token = Authy::API.verify(id: @user.authy_id, token: params[:token])

    if token.ok?
      # Mark the user as verified for get /user/:id
      @user.update(verified: true)
      # Show the user profile
      redirect_to sign_in_path
      flash[:notice] = "Your account is verified! You can login into loyosja!"
    else
      flash.now[:danger] = "Incorrect code, please try again"
      render :show_verify
    end
  end

  def resend
    @user = User.last
    Authy::API.request_sms(id: @user.authy_id)
    flash[:notice] = 'Verification code re-sent'
    redirect_to verify_path
  end

  private
    def send_message(message)
      @user = current_user
      twilio_number = ENV['TWILIO_NUMBER']
      account_sid = ENV['TWILIO_ACCOUNT_SID']
      @client = Twilio::REST::Client.new account_sid, ENV['TWILIO_AUTH_TOKEN']
    end
    def set_cart
      if session[:cart_id]
        cart = Cart.find_by(id: session[:cart_id])
        if cart.present?
          @current_cart = cart
          if current_user
            cart.update(user_id: current_user.id)
          end
        else
          session[:cart_id] = nil
        end
      end

      if session[:cart_id].nil?
        @current_cart = Cart.create(:user_id => nil)
        session[:cart_id] = @current_cart.id
      end
    end

    def set_wishlist
      if session[:wishlist_id]
        wishlist = Wishlist.find_by(id: session[:wishlist_id])
        if wishlist.present?
          @current_wishlist = wishlist
          if current_user
            wishlist.update(user_id: current_user.id)
          end
        else
          session[:wishlist_id] = nil
        end
      end

      if session[:wishlist_id].nil?
        @current_wishlist = Wishlist.create(:user_id => nil)
        session[:wishlist_id] = @current_wishlist.id
      end
    end
end
