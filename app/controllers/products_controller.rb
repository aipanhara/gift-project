class ProductsController < FrontEndApplication
  before_action :settings?

  def index
    if params[:id]
      @search = Product.where('id < ?', params[:id]).order(id: :desc).limit(16).ransack(params[:search])
      @products = @search.result
    else
      @search = Product.order(id: :desc).limit(12).ransack(params[:search])
      @products = @search.result
    end
    respond_to do |format|
      format.html
      format.js
    end
    @categories = Category.all
  end

  def show
    @product = Product.find(params[:id])
    @reviews = @product.reviews.to_a
    @review = Review.new
    @product_detail = Product.find_by_slug(params[:id])
    @categories = Category.all
    @avg_rating = if @reviews.blank?
                    0
                  else
                    @product.reviews.average(:rating)
                  end
    @total_sum_rating = @product.reviews.sum(:rating)
  end

end
