class TypesController < FrontEndApplication
  def show
    @type = ProductType.find_by_slug(params[:id])
    @products_sub = @type.products.order(id: :desc).page(params[:page]).per(12)
  end
end
