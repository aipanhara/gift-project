class FrontEndApplication < ActionController::Base
  layout "front_end_application"
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token

  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :current_cart
  helper_method :current_cart
  before_action :set_locale
  before_action :set_wishlist
  before_action :search_product
  before_action :set_order
  # respond_to :html, :xml, :json, :js

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:phone_number,:country_code,:role,:email,:password, :username, :avata])
    devise_parameter_sanitizer.permit(:account_update, keys: [:phone_number, :role, :email,:password,:password_confirmation,:current_password, :username, :avata])
  end

  def current_cart
    @current_cart ||= MyShoppingCart.new(token: cart_token)
  end

  def previous_cart
    @privious_cart = MyShoppingCart.new(token: cart_token)
  end

  def after_sign_out_path_for(resource)
    root_path
  end

  def after_sign_up_path_for(resource)
    after_sign_in_path_for(resource)
  end

  def after_sign_in_path_for(resource)
    if resource.is_a?(Admin)
      if admin_signed_in?
        admin_dashboard_path
      else
        root_path
      end
    else
      super
    end
  end

  def set_locale
    locale = I18n.available_locales.include?(params[:locale].to_sym) ? params[:locale] : I18n.locale if params[:locale].present?
    I18n.locale = locale || I18n.locale
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge(options)
  end

  private
    def cart_token
      return @cart_token unless @cart_token.nil?
      session[:cart_token] ||= SecureRandom.hex(8)
      @cart_token = session[:cart_token]
    end

      def set_wishlist
        if session[:wishlist_id]
          wishlist = Wishlist.find_by(id: session[:wishlist_id])
          if wishlist.present?
            @current_wishlist = wishlist
            if current_user
              wishlist.update(user_id: current_user.id)
            end
          else
            session[:wishlist_id] = nil
          end
        end

        if session[:wishlist_id].nil?
          if current_user
            @current_wishlist = Wishlist.create(:user_id => current_user.id)
            session[:wishlist_id] = @current_wishlist.id
          end
          @current_wishlist = Wishlist.create(:user_id => nil)
          session[:wishlist_id] = @current_wishlist.id
        end
      end

    def search_product
      @categories = Category.all
    end

    def time_ago(time)
      time
    end

    def set_order
      # @orders = Order.where("created_at > ?", time_ago(2.minute.ago))
      @search = Product.ransack(params[:search])
      @products_search = @search.result.includes(:category, :sub_category, :product_type).order("created_at desc")
      @categories = Category.all
      @galleries = Gallery.all
      @orders = Order.all
    end

    def settings?
      if Maintain.exists?
        if Maintain.first.under_maintain == true
          if Maintain.first.option == params[:controller]
            redirect_to under_construction_path
          end
        end
      end
    end

end
