class StaticPagesController < FrontEndApplication
  before_action :settings?, only: [:home, :about, :contact, :term, :faq, :privacy, :guide, :delivery]

  def home
    @products = Product.all.order(:position, :id => :desc).limit(6)
    @features = Product.where(filtered: "featured").order(:position, id: :desc).limit(9)
    @categories = Category.all
    @galleries = Gallery.all
  end


  def about
    @categories = Category.all
   end

  def contact
    @categories = Category.all
  end

  def term
      @categories = Category.all
   end

  def faq
      @categories = Category.all
    end

  def privacy
    @categories = Category.all
  end

  def cancellation
    @categories = Category.all
  end

  def guide
    @categories = Category.all
  end

  def delivery
    @categories = Category.all
  end

  def under_construction

  end
end
