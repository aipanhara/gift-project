class Admin::SettingsController < ApplicationController
  before_action :authenticate_admin!
  def new
    @setting = Setting.new
    if Setting.any?
      redirect_to edit_admin_setting_url(Setting.first)
    else
      5.times do
        @maintain = @setting.maintains.build
      end
    end
  end

  def create
    @setting = Setting.new(setting_params)

    if @setting.save
      redirect_to edit_admin_setting_path(@setting)
    else
      render :new
    end
  end

  def edit
    if Setting.exists?
      @setting = Setting.find(params[:id])
      if !@setting.maintains.exists?
        5.times do
          @maintain = @setting.maintains.build
        end
      end
    else
      redirect_to new_admin_setting_path
    end
  end

  def update
    @setting = Setting.find(params[:id])

    if @setting.update(setting_params)
      redirect_to edit_admin_setting_url(@setting), notice: 'Successfully updated setting'
    else
      flash[:alert] = 'There was a problem updating setting'
  		render :edit
    end
  end

  private
    def setting_params
      params.require(:setting).permit!
    end
end
