class Admin::ProductTypesController < ApplicationController
  before_action :authenticate_admin!
  before_action :admin_only?, except: [:index]
  before_action :delivery_only?
  def index
    @product_type = current_admin.product_types.build
    @product_types = ProductType.all.order(id: :desc)
  end

  def show
    @product_type = ProductType.find(params[:id])
  end

  def new

  end

  def create
    @product_type = current_admin.product_types.build(product_type_params)
    if @product_type.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_product_types_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or product_type Duplicated"
        format.html { redirect_to admin_product_types_path }
        format.js { render template: "admin/product_types/product_type_error.js.erb" }
      end
    end
  end

  def edit
    @product_type = ProductType.find(params[:id])
  end

  def update
    @product_type = ProductType.find(params[:id])
    if @product_type.update(product_type_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_product_types_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or product_type Duplicated"
        format.html { redirect_to admin_product_types_path }
        format.js { render template: "admin/product_types/product_type_error.js.erb" }
      end
    end
  end

  def destroy
    @product_type = ProductType.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_product_types_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def delivery_only?
      unless !current_admin.delivery?
        unless @admin == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def product_type_params
      params.required(:product_type).permit!
    end
end
