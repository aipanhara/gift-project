class Admin::GalleriesController < ApplicationController
  before_action :authenticate_admin!
  before_action :admin_only?, except: [:index]
  before_action :delivery_only?
  def index
    @gallery = current_admin.galleries.build
    @galleries = Gallery.all.order(id: :desc)
  end

  def show
    @gallery = Gallery.find(params[:id])
  end

  def new

  end

  def create
    @gallery = current_admin.galleries.build(galleries_params)
    if @gallery.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_galleries_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or category Duplicated"
        format.html { redirect_to admin_galleries_path }
        format.js { render template: "admin/galleries/gallery_error.js.erb" }
      end
    end
  end

  def edit
    @gallery = Gallery.find(params[:id])
  end

  def update
    @gallery = Gallery.find(params[:id])
    if @gallery.update(galleries_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_galleries_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or category Duplicated"
        format.html { redirect_to admin_galleries_path }
        format.js { render template: "admin/galleries/gallery_error.js.erb" }
      end
    end
  end

  def destroy
    @gallery = Gallery.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_galleries_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def delivery_only?
      unless !current_admin.delivery?
        unless @admin == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def galleries_params
      params.required(:gallery).permit!
    end
end
