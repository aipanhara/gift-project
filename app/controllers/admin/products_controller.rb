class Admin::ProductsController < ApplicationController
  before_action :authenticate_admin!
  before_action :admin_only?
  before_action :delivery_only?

  def index
    @product = current_admin.products.build
    if !@product.assets.exists?
      @asset = @product.assets.build
    end
    @products = Product.all.order(id: :desc)
  end

  def sort_product
    @products = Product.all.order(:position, id: :desc)
  end

  def sort_product_featured
    @products = Product.where(filtered: "featured").order(:position, id: :desc)
  end

  def sort
    params[:product].each_with_index do |id, index|
      Product.where(id: id).update_all(position: index + 1)
    end
  end

  def show
    @product = Product.find(params[:id])
  end

  def new

  end

  def create
    params[:product][:remaining_stock_quantity] = params[:product][:stock_quantity]
    @product = current_admin.products.build(product_params)
    if @product.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_products_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or product Duplicated"
        format.html { redirect_to admin_products_path }
        format.js { render template: "admin/products/product_error.js.erb" }
      end
    end
  end

  def edit
    @product = Product.find(params[:id])
    if !@product.assets.exists?
      @asset = @product.assets.build
    else
      @product = Product.find(params[:id])
    end
  end

  def update
    @product = Product.find(params[:id])
    params[:product][:track_quantity] = params[:product][:edit_quantity]
    if @product.update(product_params)
      @product.update_columns(
        stock_quantity: @product.edit_quantity + @product.stock_quantity,
        remaining_stock_quantity: @product.edit_quantity + @product.remaining_stock_quantity
      )
      @product.update_columns(track_quantity: @product.edit_quantity)
      @product.update_columns(edit_quantity: 0)
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_products_path }
        format.js
      end
    else
      respond_to do |format|
        p @product.errors.full_messages
        flash.now[:alert] = "Please fill the field blank or Duplicated"
        format.html { redirect_to admin_products_path }
        format.js { render template: "admin/products/product_error.js.erb" }
      end
    end
  end

  def destroy
    @product = Product.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_products_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def delivery_only?
      unless !current_admin.delivery?
        unless @admin == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def product_params
      params.required(:product).permit(
        :product_name,
        :product_price,
        :stock_quantity,
        :edit_quantity,
        :remaining_stock_quantity,
        :admin_id, :slug,
        :category_id,
        :sub_category_id,
        :product_type_id,
        :description,
        :status,
        :filtered,
        assets_attributes: [:id, :_destroy, :image, :product_id, :status]
      )
    end

end
