class Admin::DashboardsController < ApplicationController
  before_action :authenticate_admin!

  def index
    @products = Product.all.count
    @customers = User.all.count
    @orders = Order.all.where(status: true).count
    @admin = Admin.all.count
    @completed_order = Order.all
  end

end
