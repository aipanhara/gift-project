class Admin::LineItemsController < ApplicationController
  before_action :authenticate_admin!
  before_action :admin_only?
  before_action :delivery_only?
  def index
    @line_item = current_admin.line_items.build
    @line_items = LineItem.all
  end

  def show
    @line_item = LineItem.find(params[:id])
  end

  def edit
    @line_item = LineItem.find(params[:id])
  end

  def update
    @line_item = LineItem.find(params[:id])
    if @line_item.update(line_item_params)
      if @line_item.status == true
        @line_item.product.update_columns(remaining_stock_quantity: @line_item.product.remaining_stock_quantity - @line_item.quantity)
      end
      redirect_to admin_line_item_path(@line_item), notice: "Successful Updated"
    else
      render :edit
    end

  end

  def destroy
    @line_item = LineItem.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_line_items_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def delivery_only?
      unless !current_admin.delivery?
        unless @admin == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def line_item_params
      params.required(:line_item).permit!
    end

end
