class Admin::SubCategoriesController < ApplicationController
  before_action :authenticate_admin!
  before_action :admin_only?, except: [:index]
  before_action :delivery_only?

  def index
    @sub_category = current_admin.sub_categories.build
    @sub_categories = SubCategory.all.order(id: :desc)
  end

  def show
    @sub_category = SubCategory.find(params[:id])
  end

  def new

  end

  def create
    @sub_category=current_admin.sub_categories.build(sub_category_params)
    if @sub_category.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_sub_categories_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or sub category Duplicated"
        format.html { redirect_to admin_sub_categories_path }
        format.js { render template: "admin/sub_categories/sub_category_error.js.erb" }
      end
    end
  end

  def edit
    @sub_category = SubCategory.find(params[:id])
  end

  def update
    @sub_category = SubCategory.find(params[:id])
    if @sub_category.update(sub_category_params)
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_sub_categories_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or sub category Duplicated"
        format.html { redirect_to admin_sub_categories_path }
        format.js { render template: "admin/sub_categories/sub_category_error.js.erb" }
      end
    end
  end

  def destroy
    @sub_category = SubCategory.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_sub_categories_url }
      format.js
    end
  end


  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def delivery_only?
      unless !current_admin.delivery?
        unless @admin == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def sub_category_params
      params.required(:sub_category).permit!
    end
end
