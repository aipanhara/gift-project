class Admin::OrdersController < ApplicationController
  before_action :authenticate_admin!
  before_action :admin_only?, except: [:index]
  before_action :delivery_only?, except: [:edit, :update]
  before_action :handle_update_status, only: [:update]

  def get_all_orders_report
    @orders = Order.where(status: true)
  end

  def seven_days_ago_report
    @orders = Order.where(sold_date: 7.days.ago)
  end

  def invoice
    @invoice = Order.find(params[:id])
  end

  def index
    @search_order = Order.ransack(params[:search])
    @orders = @search_order.result.where.not(:total_amount => 0.0).order(id: :desc).page(params[:page]).per(20)
  end

  def search
    index
    render :index
  end

  def show
    @order = Order.find(params[:id])
  end

  def new

  end

  def create
    @order = current_admin.orders.build(order_params)
    if @order.save
      respond_to do |format|
        flash.now[:notice] = "Successful Created"
        format.html { redirect_to admin_orders_path }
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Please fill the field blank or order Duplicated"
        format.html { redirect_to admin_orders_path }
        format.js { render template: "admin/categories/order_error.js.erb" }
      end
    end
  end

  def edit
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])

    if @order.update(order_params)
      @order.line_items.each do |item|
        if @order.status == true
          item.product.update_columns(remaining_stock_quantity: item.product.remaining_stock_quantity - item.quantity)
          if params[:order][:discount].to_i == 0.0 && params[:order][:delivery_fee].to_i == 0.0
            @order.update(profit: 0.0, discount: 0.0, delivery_fee: 0.0)
          elsif params[:order][:discount] != 0.0 && params[:order][:delivery_fee] != 0.0
            profit = (@order.sub_total.to_i - (params[:order][:discount].to_i + params[:order][:delivery_fee].to_i))
            @order.update(profit: profit, discount: params[:order][:discount], delivery_fee: params[:order][:delivery_fee])
          elsif params[:order][:discount] != 0.0
            profit = (@order.sub_total.to_i - params[:order][:discount].to_i)
            @order.update(profit: profit, discount: params[:order][:discount], delivery_fee: params[:order][:delivery_fee])
          elsif params[:order][:delivery_fee] != 0.0
            profit = (@order.sub_total.to_i - params[:order][:delivery_fee].to_i)
            @order.update(profit: profit, discount: params[:order][:discount], delivery_fee: params[:order][:delivery_fee])
          end
        end
      end
      respond_to do |format|
        flash.now[:notice] = "Successful Updated"
        format.html { redirect_to admin_orders_path }
        format.js
      end
    else
      render :edit
    end
  end

  def destroy
    @order = Order.destroy(params[:id])
    respond_to do |format|
      flash.now[:error] = "Delete"
      format.html { redirect_to admin_orders_url }
      format.js
    end
  end

  private
    def admin_only?
      unless  current_admin.admin?
        unless @user == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def delivery_only?
      unless !current_admin.delivery?
        unless @admin == current_admin
          redirect_to admin_dashboard_path, alert: "Access Denied"
        end
      end
    end

    def order_params
      params.required(:order).permit(:first_name, :last_name, :phone_num, :description, :status)
    end

    def handle_update_status
      @order = Order.find(params[:id])
      if @order.status == true
        respond_to do |format|
          format.html { redirect_to admin_orders_path }
          format.js { render template: "admin/orders/handle_update_status.js.erb" }
          flash.now[:alert] = "You cant do that!"
        end
      end
    end

end
