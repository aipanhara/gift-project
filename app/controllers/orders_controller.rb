class OrdersController < FrontEndApplication
  before_action :authenticate_user!, only: [:index, :show, :new, :create, :profile]
  before_action :cart_is_empty, only: [:new, :create]

  def profile
    @orders_status = current_user.orders.page(params[:page]).per(10).where("total_amount != 0")
    @categories = Category.all
  end

  def index
    @orders = Order.all
  end

  def show
    @order = Order.find(params[:id])
    @items = current_cart.order.line_items
  end

  def new
    @search = Product.ransack(params[:search])
    @products = @search.result
    @order = current_cart.order
    @categories = Category.all
  end

  def create
    @order = current_cart.order(order_params)
    @order.update(user_id: current_user.id)
    client = Slack::Web::Client.new
    client.auth_test
    if @order.save
      @order.update(sold_date: Time.now, total_amount: @cart.sub_total)
      client.chat_postMessage(
        channel: '#preorder',
        text: "http://loyosja.com/admin/orders/#{@order.id}/edit",
        as_user: true
      )
      session[:cart_token] = nil
      redirect_to profiles_path
      flash[:notice] = "Thank for your order"
    else
      flash[:alert] = "Please fill the bill infomation"
      render :new
    end
  end

  def destroy
    @order = Order.find(params[:id])
    @order.destroy
    redirect_to profiles_path
    flash[:notice] = "Order has been cancelled"
  end

  def edit
    @order = Order.find(params[:id])
  end

  def update
    @order = Order.find(params[:id])
    @order.update(user_id: current_user.id)
    client = Slack::Web::Client.new
    client.auth_test
    if @order.update(order_params)
      @order.update(sold_date: Time.now, total_amount: current_cart.order.sub_total)
      client.chat_postMessage(
        channel: '#preorder',
        text: "http://loyosja.com/admin/orders/#{@order.id}/edit",
        as_user: true
      )
      session[:cart_token] = nil
      redirect_to profiles_path
      flash[:notice] = "Thank for your order"
    else
      flash[:alert] = "Please fill the bill infomation"
      render :edit
    end
  end

  def cart_is_empty
    return unless current_cart.items_count == 0
    flash[:alert] = "You cart is empty"
    redirect_to cart_path
  end

  private

    def order_params
      params.require(:order).permit(
        :user_id,
        :first_name,
        :last_name,
        :address_one,
        :description,
        :sold_date,
        :phone_num,
        line_items_attributes:[:id, :product_id]
      )
    end
end
