class WishlistLineItemsController < FrontEndApplication
  before_action :authenticate_user!, only: [:create, :destroy, :add_quantity, :reduce_quantity]

  def create
    chosen_product = Product.find(params[:product_id])
    @product = Product.find(params[:product_id])
    add_items_to_wishlist(chosen_product)
    respond_to do |format|
      if @wishlist_line_item.save!
        @wishlist_line_item.update(user_id: current_user.id)
        flash.now[:notice] = "#{@wishlist_line_item.product.product_name} Added To Wishlist"
        format.html { redirect_back(fallback_location: @current_wishlist)}
        format.js
      else
        format.html { render :new, notice: "Error Adding Product To Basket!" }
        flash.now[:alert] = "Please fill the field blank or product Duplicated"
        format.js { render template: "wishlist_line_items/wishlist_line_item_error.js.erb" }
      end
    end
  end

  def destroy
    @wishlist_line_item = WishlistLineItem.destroy(params[:id])
    respond_to do |format|
      flash.now[:alert] = "#{@wishlist_line_item.product.product_name}  was removed from the wishlist!"
      format.html { redirect_to @current_wishlist }
      format.js
    end
  end

  def add_quantity
    @wishlist_line_item = WishlistLineItem.find(params[:id])
    @wishlist_line_item.save
    redirect_back(fallback_location: @current_wishlist)
  end

  def reduce_quantity
    @wishlist_line_item = WishlistLineItem.find(params[:id])
    if @wishlist_line_item.quantity > 1
      @wishlist_line_item.quantity -= 1
      @wishlist_line_item.save
      redirect_back(fallback_location: @current_wishlist)
    elsif @wishlist_line_item.quantity = 1
      destroy
    end
  end

  private
    def wishlist_line_item_params
      params.require(:wishlist_line_item).permit(:id,:quantity, :product_id, :wishlist_id, :order_id, :user_id)
    end

    def add_items_to_wishlist(chosen_product)
      if @current_wishlist.products.include?(chosen_product)
        @wishlist_line_item = @current_wishlist.wishlist_line_items.find_by(product_id: chosen_product)
      else
        @wishlist_line_item = WishlistLineItem.new
        @wishlist_line_item.wishlist = @current_wishlist
        @wishlist_line_item.product = chosen_product
      end
    end

end
