class LineItemsController < FrontEndApplication
  before_action :authenticate_user!, only: [:history]

  def index
    @items = current_cart.order.line_items
  end

  def create
    @line_item = current_cart.add_item(
      product_id: params[:product_id],
      quantity: params[:quantity],
      color: params[:color],
      shirt_size: params[:shirt_size],
      phone_type: params[:phone_type]
    )
    if current_user
      current_cart.order.line_items.update(user_id: current_user.id)
    else
      current_cart.order.line_items.update(user_id: nil)
    end

    if @line_item
      respond_to do |format|
        flash.now[:notice] = "Product Has Been Added To Cart"
        format.html { redirect_back(fallback_location: cart_path)}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "Quanity must be greater that 0 and must not a nagative number"
        format.js { render template: "line_items/line_item_error.js.erb" }
      end
    end
  end

  def history
    @line_items = current_user.line_items
  end

  def destroy
    @line_item = current_cart.remove_item(id: params[:id])
    respond_to do |format|
      flash.now[:alert] = "Product Has Been Removed"
      format.html { redirect_back(fallback_location: cart_path)}
      format.js
    end
  end

  def cancel_item
    @order = Order.find(params[:order_id])
    @line_item = @order.line_items.find(params[:id])
    if @order.status == false
      @line_item.destroy
      total = @order.line_items.sum('quantity * price')
      @order.update_columns(total_amount: total)
      if total == 0
        @order.destroy
      end
      respond_to do |format|
        flash.now[:alert] = "Product Has Been Removed"
        format.html { redirect_back(fallback_location: profiles_path)}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "You cannot cancel your order because you are paid!"
        format.html { redirect_back(fallback_location: profiles_path)}
        format.js
      end
    end
  end

  def update_quantity
    @order = Order.find(params[:order_id])
    @line_item = @order.line_items.find(params[:id])
    if @order.status == false
      qty = params[:quantity].to_d
      @line_item.update_columns(quantity: qty)
      total = @order.line_items.sum('quantity * price')
      @order.update_columns(total_amount: total)
      respond_to do |format|
        flash.now[:success] = "Updated Quanity"
        format.html { redirect_back(fallback_location: profiles_path)}
        format.js
      end
    else
      respond_to do |format|
        flash.now[:alert] = "You cannot do that due to your product is already delivered!"
        format.html { redirect_back(fallback_location: profiles_path)}
        format.js { render template: "line_items/update_error.js.erb" }
      end
    end
  end


end
