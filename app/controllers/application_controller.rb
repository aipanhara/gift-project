class ApplicationController < ActionController::Base
  layout "application"
  protect_from_forgery with: :exception

  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token
  before_action :configure_permitted_parameters, if: :devise_controller?

  before_action :set_locale
  before_action :search_product
  before_action :set_order
  before_action :newOrder
  # respond_to :html, :xml, :json, :js

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:phone_number,:country_code,:role,:email,:password, :username, :avata])
    devise_parameter_sanitizer.permit(:account_update, keys: [:phone_number, :role, :email,:password,:password_confirmation,:current_password, :username, :avata])
  end

  def newOrder
    @new_order = Order.where("status = false AND sold_date")
  end

  def after_sign_out_path_for(resource)
    root_path
  end

  def after_sign_up_path_for(resource)
    after_sign_in_path_for(resource)
  end

  def after_sign_in_path_for(resource)
    if resource.is_a?(Admin)
      if admin_signed_in?
        admin_dashboard_path
      else
        root_path
      end
    else
      super
    end
  end

  def set_locale
    locale = I18n.available_locales.include?(params[:locale].to_sym) ? params[:locale] : I18n.locale if params[:locale].present?
    I18n.locale = locale || I18n.locale
  end

  def default_url_options(options = {})
    { locale: I18n.locale }.merge(options)
  end

  private

    def search_product
      @cart = @current_cart
    end

    def time_ago(time)
      time
    end

    def set_order
      @orders = Order.all
    end
end
