class SubCategoriesController < FrontEndApplication
  def show
    @subcategory = SubCategory.find_by_slug(params[:id])
    @products_sub = @subcategory.products.order(id: :desc).page(params[:page]).per(12)
  end
end
