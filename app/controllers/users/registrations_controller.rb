# frozen_string_literal: true
class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]
  before_action :set_cart
  # GET /resource/sign_up
  layout "application"
  def new
    super
  end

  # POST /resource
  def create
    @user = User.new(user_params)
    if @user.save
      session[:user_id] = @user.id

      # Create user on Authy, will return an id on the object
      authy = Authy::API.register_user(
        email: @user.email,
        cellphone: @user.phone_number,
        country_code: @user.country_code
      )
      @user.update(authy_id: authy.id)

      # Send an SMS to your user
      Authy::API.request_sms(id: @user.authy_id)
      redirect_to verify_path
      flash[:notice] = "6 digits is sent your phone, Verfify your code here!"
    else
      render :new
    end
    # Save the user_id to the session object
  end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:country_code, :phone_number])
  end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   redirect_to verify_path
  # end
  def user_params
    params.require(:user).permit(:email, :phone_number, :password, :password_confirmation, :country_code, :avata)
  end
  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
  def show_verify
    @search = Product.ransack(params[:search])
    @products = @search.result.includes(:category, :sub_category, :product_type)
    @categories = Category.all
    return redirect_to new_user_registration_path  unless session[:user_id]
  end

  private
    def set_cart
      if session[:cart_id]
        cart = Cart.find_by(id: session[:cart_id])
        if cart.present?
          @current_cart = cart
          if current_user
            cart.update(user_id: current_user.id)
          end
        else
          session[:cart_id] = nil
        end
      end

      if session[:cart_id].nil?
        @current_cart = Cart.create(:user_id => nil)
        session[:cart_id] = @current_cart.id
      end
    end

    def set_wishlist
      if session[:wishlist_id]
        wishlist = Wishlist.find_by(id: session[:wishlist_id])
        if wishlist.present?
          @current_wishlist = wishlist
          if current_user
            wishlist.update(user_id: current_user.id)
          end
        else
          session[:wishlist_id] = nil
        end
      end

      if session[:wishlist_id].nil?
        @current_wishlist = Wishlist.create(:user_id => nil)
        session[:wishlist_id] = @current_wishlist.id
      end
    end

end
