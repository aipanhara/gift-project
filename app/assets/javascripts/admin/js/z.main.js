$(function(){
  $('#start_date').daterangepicker({
    locale: { format: 'YYYY-MM-DD'},
    opens: "left",
    singleDatePicker: true,
    showDropdowns: true
  });

  $('#start_date').on('cancel.daterangepicker', function(){
    $(this).val(' ');
  });

  $('#end_date').daterangepicker({
    locale: { format: 'YYYY-MM-DD'},
    opens: "left",
    singleDatePicker: true,
    showDropdowns: true
  });

  $('#end_date').on('cancel.daterangepicker', function(){
    $(this).val(' ');
  });
});
