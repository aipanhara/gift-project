class ProductType < ApplicationRecord
  before_save :update_slug

  belongs_to :admin
  has_many :products

  validates :type_name,
             uniqueness: { case_sensitive: true },
             presence: true,
             length: { maximum: 20 }

  def update_slug
    self.slug = type_name.parameterize
  end

end
