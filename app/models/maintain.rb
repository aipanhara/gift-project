class Maintain < ApplicationRecord
  belongs_to :setting

  SELECT_PAGE = [
    "None",
    "static_pages",
    "products",
    "carts",
    "categories",
    "sub_categories"
  ]

end
