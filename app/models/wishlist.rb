class Wishlist < ApplicationRecord
  belongs_to :user, optional: true
  has_many :wishlist_line_items
  has_many :products, through: :wishlist_line_items
end
