class Backup < ApplicationRecord
  belongs_to :line_item, optional: true
  belongs_to :order, optional: true
  belongs_to :product, optional: true
  belongs_to :user, optional: true

  def total_price
    if valid_quantity_and_price?
      quantity.to_s.to_d * product.product_price.to_s.to_d
    else
      0.0
    end
  end

  def valid_quantity_and_price?
    !quantity.to_s.strip.empty? && !product.product_price.to_s.strip.empty?
  end
end
