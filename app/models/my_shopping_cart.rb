class MyShoppingCart

  def initialize(token:)
    @token = token
  end

  def order
    @order ||= Order.find_or_create_by(token: @token) do |order|
      order.sub_total = 0
    end
  end

  def items_count
    order.line_items.sum(:quantity)
  end

  def add_item(product_id:, quantity: 1, color:, shirt_size:, phone_type:)
    product = Product.find(product_id)

    line_item = order.line_items.find_or_create_by(
      product_id: product_id
    )

    line_item.price = product.product_price
    line_item.quantity = line_item.quantity.to_i + quantity.to_i
    line_item.color = color
    line_item.shirt_size = shirt_size
    line_item.phone_type = phone_type

    if line_item.quantity > product.remaining_stock_quantity
      false
    else
      if line_item.save
        update_sub_total!
      else
        false
      end
    end

  end

  def remove_item(id:)
    order.line_items.destroy(id)
    update_sub_total!
  end

  def cancel_items(id:)
    order.line_items.destroy(id)
    update_sub_total
  end

  private
    def update_sub_total!
      order.sub_total = order.line_items.sum('quantity * price')
      order.total_amount = order.sub_total
      order.save
    end

end
