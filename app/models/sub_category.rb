class SubCategory < ApplicationRecord
  belongs_to :category, optional: true
  has_many :products
  has_many :sub_categories, through: :products

  belongs_to :admin
  before_save :update_slug
  validates :sub_category_name,
             uniqueness: { case_sensitive: true },
             presence: true,
             length: { maximum: 20 }


  def update_slug
    self.slug = sub_category_name.parameterize
  end

end
