class Setting < ApplicationRecord
  has_many :maintains
  accepts_nested_attributes_for :maintains, :allow_destroy => true

end
