class Product < ApplicationRecord
  SELECT_FILTERED_OPTIONS = [
    "None",
    "Featured",
    "Most Popular",
    "Trending",
    "What's New",
    "Top 50"
  ]

  before_save :update_slug
  belongs_to :admin
  belongs_to :category
  belongs_to :product_type, optional: true
  belongs_to :sub_category
  has_many :assets
  accepts_nested_attributes_for :assets, :reject_if => :all_blank, :allow_destroy => true
  has_many :line_items, dependent: :destroy
  has_many :orders, through: :line_items
  has_many :wishlist_line_items, dependent: :destroy
  has_many :orders, through: :wishlist_line_items
  has_many :reviews
  has_many :backups

  # validates :product_name, presence: true, length: { maximum: 20 }
  validates :product_price, presence: true, numericality: { greater_than: 0 }
  validates :stock_quantity, presence: true, numericality: { greater_than: 0 }

  def update_slug
    self.slug = product_name.parameterize
  end

end
