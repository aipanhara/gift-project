class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable

  after_initialize :set_default_role, :if => :new_record?

  attr_writer :login

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :omniauthable, authentication_keys: [:login]

  has_attached_file :avata, style: { medium: "300x500>",thumb: "100x100>" }
  validates_attachment_content_type :avata, content_type: /\Aimage\/.*\z/
  validates :phone_number, uniqueness: true
  validates :phone_number, presence: true
  validates :phone_number, numericality: true
  validates_format_of :username, with: /^[a-zA-Z0-9_\.]*$/, :multiline => true

  has_many :orders, dependent: :destroy
  has_many :carts
  has_many :line_items
  has_many :backups
  belongs_to :admin, optional: true
  has_many :wishlists
  has_many :services, dependent: :destroy
  has_many :wishlist_line_items
  def login
    @login || self.phone_number
  end

  enum role: [:user, :seller, :customer]

  def set_default_role
    self.role ||= :user
  end

  def self.find_for_database_authentication(warden_conditions)
      conditions = warden_conditions.dup
      if login = conditions.delete(:login)
        where(conditions.to_h).where(["lower(phone_number) = :value", { :value => login.downcase }]).first
      elsif conditions.has_key?(:phone_number)
        where(conditions.to_h).first
      end
    end

end
