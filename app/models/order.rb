class Order < ApplicationRecord

  has_many :line_items, dependent: :destroy
  has_many :backups
  belongs_to :user, optional: true
  belongs_to :admin, optional: true
  has_many :wishlist_line_items, dependent: :destroy

  scope :report_between, ->(start_date, end_date) { where(created_at: start_date..end_date) }

end
