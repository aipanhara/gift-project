class WishlistLineItem < ApplicationRecord
  belongs_to :product
  belongs_to :wishlist
  belongs_to :order, optional: true
  belongs_to :user, optional: true

end
