class Category < ApplicationRecord
  before_save :update_slug
  validates :category_name,
             uniqueness: { case_sensitive: true },
             presence: true,
             length: { maximum: 20 }

  has_many :products
  has_many :sub_categories, through: :products
  belongs_to :admin
  validates :category_name, presence: true

  def update_slug
    self.slug = category_name.parameterize
  end

end
