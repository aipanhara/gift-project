class LineItem < ApplicationRecord
  belongs_to :product, optional: true
  belongs_to :order, optional: true
  belongs_to :user, optional: true
  belongs_to :admin, optional: true
  has_many :backups

  validates :quantity, numericality: { greater_than: 0 }
end
