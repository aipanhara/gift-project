Rails.application.routes.draw do
    root 'static_pages#home'
    resources :home

    # devise_for :admins, :controllers => { :registrations => :registrations }
    devise_for :admins, :skip => [:registrations]

    devise_scope :admin do
      get "/signin" => "devise/sessions#new", :as => "signin" # custom path to login/sign_in
    end

    as :admin do
      get 'admins/edit' => 'devise/registrations#edit', :as => 'edit_admin_registration'
      put 'admins' => 'devise/registrations#update', :as => 'admin_registration'
      patch 'admins' => 'devise/registrations#update', :as => 'update_admin_registration'
    end


    namespace :admin do
      root "dashboards#index"
      get 'dashboards' => "dashboards#index", :as => "dashboard"
      get 'invoices/:id' => 'orders#invoice', as: "invoice"
      get "get_all_orders_report" => "orders#get_all_orders_report"
      get "seven_days_ago_report" => "orders#seven_days_ago_report"
      get "reports" => "reports#index"

      resources :products do
        collection do
          patch :sort
          get "sort_product" => "products#sort_product", as: "sort_product"
          get "sort_product_featured" => "products#sort_product_featured", as: "sort_product_featured"
        end
      end



      resources :categories
      resources :sub_categories
      resources :product_types
      resources :galleries
      resources :orders do
        collection do
          get "search" => "orders#search", vai: [:get, :post], as: :search
        end
      end
      resources :users
      resources :admins
      resources :line_items
      resources :settings, only: [:index, :new, :update, :create, :edit]

      get "testorder" => "orders#testorder"
    end

    resources :sub_categories, only: [:show]
    resources :categories, only: [:show]
    resources :wishlists
    resources :products do
      resources :reviews
    end

    devise_for :users, controllers: {
      sessions: 'users/sessions',
      registrations: "users/registrations",
      omniauth_callbacks: "users/omniauth_callbacks",
      passwords: 'users/passwords'
    }

    devise_scope :user do
      get "/sign_in" => "devise/sessions#new", :as => "sign_in" # custom path to login/sign_in
      get "/sign_up" => "devise/registrations#new", :as => "sign_up" # custom path to login/sign_in
      get "users/verify/" => 'users/registrations#show_verify', as: 'verify'
      post "users/verify"
      post "users/resend"
    end

    resources :orders do
      delete "cancel/:id" => "line_items#cancel_item", as: "cancel_line_item"
      post "update_quantity/:id" => "line_items#update_quantity", as: "update_quantity"
    end
    resources :product_types, only: [:index, :show]
    resources :types, only: [:show]
    get 'profiles' => "orders#profile", :as => "profiles"
    post 'wishlist_line_items/:id/add' => 'wishlist_line_items#add_quantity', as: 'wishlist_line_item_add'
    post 'wishlist_line_items/:id/reduce' => 'wishlist_line_items#reduce_quantity', as: 'wishlist_line_item_reduce'
    post 'wishlist_line_items' => 'wishlist_line_items#create'
    get 'wishlist_line_items/:id' => 'wishlist_line_items#shiow', as: "wishlist_line_item"
    delete 'wishlist_line_items/:id' => 'wishlist_line_items#destroy'
    get 'abouts' => "static_pages#about", :as => "abouts"
    get 'deliveries' => "static_pages#delivery", :as => "deliveries"
    get 'cancellation' => "static_pages#cancellation", :as => "cancellation"

    get 'cart' => "line_items#index"
    get 'history' => "line_items#history", :as => "history"
    resources :line_items, path: "/cart/items"
    get  'cart/checkout' => "orders#new", :as => "checkout"
    patch 'cart/checkout' => "orders#create"
    get 'under_construction' => "static_pages#under_construction", as: "under_construction"

end
